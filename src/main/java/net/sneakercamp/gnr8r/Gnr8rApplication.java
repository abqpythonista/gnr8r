package net.sneakercamp.gnr8r;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gnr8rApplication {

	public static void main(String[] args) {
		SpringApplication.run(Gnr8rApplication.class, args);
	}

}
